package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {
    TextView next;
    EditText email;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        next = findViewById(R.id.next);
        email = findViewById(R.id.email);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emailAddress = email.getText().toString();
                if(emailAddress.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()){
                    email.setError("Invalid email");
                }else{
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("email", emailAddress);
                    editor.apply();
                    Intent i = new Intent(RegisterActivity.this, PasswordRegister.class);
                    startActivity(i);
                }
            }
        });
    }
}
package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rt241.citytourplanner.Models.PreviousTourModel;
import com.rt241.citytourplanner.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PreviousTourAdapter extends RecyclerView.Adapter<PreviousTourAdapter.ViewHolder> {

    private List<PreviousTourModel> previousTourModelList;
    private Context context;

    public PreviousTourAdapter(List<PreviousTourModel> previousTourModelList, Context context) {
        this.previousTourModelList = previousTourModelList;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.previous_tour_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PreviousTourModel previousTourModel = previousTourModelList.get(position);
        Picasso.get().load(previousTourModel.getCityModel().getPhoto()).into(holder.image);
        holder.place.setText(previousTourModel.getCity());
    }

    @Override
    public int getItemCount() {
        return previousTourModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView place;
        private TextView date;
        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);
            place = view.findViewById(R.id.place);
        }
    }
}

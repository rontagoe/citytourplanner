package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rt241.citytourplanner.LocationDetails;
import com.rt241.citytourplanner.Models.Favorite;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.R;
import com.rt241.citytourplanner.helper.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RecommendedPlaceAdapter extends RecyclerView.Adapter<RecommendedPlaceAdapter.HomeViewHolder> {


    private List<Place> homeModelList;
    private Context context;

    private List<Favorite> favouriteList = new ArrayList<>();

    private SharedPreferences prefs;
    SharedPreferences.Editor edit;

    private DatabaseHelper db;

    public RecommendedPlaceAdapter(List<Place> homeModelList, Context context) {
        this.homeModelList = homeModelList;
        this.context = context;
        notifyDataSetChanged();

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        edit = prefs.edit();

        db = new DatabaseHelper(context);
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommended_place_item, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeViewHolder holder, final int position) {

        Favorite favourite = null;
        final Place homeModel = homeModelList.get(position);

        if (db.isFavourite(String.valueOf(homeModel.getId()))) {
            holder.imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_24px));
        } else {
            holder.imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_outline));
        }

        Picasso.get().load(homeModel.getPhoto()).into(holder.imagePlace);
        holder.place.setText(homeModel.getCity() == null ? "-" : homeModel.getCity().getName()+ ", "+ homeModel.getCity().getCountry());
        holder.city.setText(homeModel.getName().isEmpty()||homeModel.getName() == null ? "-" : homeModel.getName());
        /*holder.addToItinerary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LocationDetails.class);
                intent.putExtra("id",String.valueOf(homeModel.getId()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });*/
        /*holder.imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (db.isFavourite(String.valueOf(homeModel.getId()))) {
                    deleteFavorite(homeModel,position);
                } else {
                    postFavorite(homeModel,position);
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return homeModelList.size();
    }

    class HomeViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagePlace;
        private ImageView imgFav;
        private TextView place;
        private TextView city;
        private TextView addToItinerary;
        public HomeViewHolder(View view) {
            super(view);
            imagePlace = view.findViewById(R.id.imagePlace);
            imgFav = view.findViewById(R.id.fav);
            place = view.findViewById(R.id.country);
            addToItinerary = view.findViewById(R.id.addToItinerary);
            city = view.findViewById(R.id.city);
        }
    }
}

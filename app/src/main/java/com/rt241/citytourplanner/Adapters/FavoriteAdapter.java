package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rt241.citytourplanner.LocationDetails;
import com.rt241.citytourplanner.Models.Favorite;
import com.rt241.citytourplanner.R;
import com.rt241.citytourplanner.helper.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {


    private List<Favorite> homeModelList;
    private Context context;

    private DatabaseHelper db;

    public FavoriteAdapter(List<Favorite> homeModelList, Context context) {
        this.homeModelList = homeModelList;
        this.context = context;

        db = new DatabaseHelper(context);
    }

    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favorite_list_row, parent, false);
        return new FavoriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavoriteViewHolder holder, int position) {
        Favorite homeModel = homeModelList.get(position);

        if (db.isFavourite(String.valueOf(homeModel.getId()))) {
            holder.imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_24px));
        } else {
            holder.imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_outline));
        }

        Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(holder.imagePlace);
        holder.city.setText(homeModel.getPlace());
        holder.place.setText(homeModel.getCity());
        holder.addToItinerary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LocationDetails.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return homeModelList.size();
    }

    class FavoriteViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagePlace;
        private TextView place;
        private ImageView imgFav;
        private TextView city;
        private LinearLayout addToItinerary;
        public FavoriteViewHolder(View view) {
            super(view);
            imagePlace = view.findViewById(R.id.imagePlace);
            imgFav = view.findViewById(R.id.fav);
            place = view.findViewById(R.id.place);
            addToItinerary = view.findViewById(R.id.addToItinerary);
            city = view.findViewById(R.id.city);
        }
    }
}

package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rt241.citytourplanner.ItenerayDetailsActivity;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.Models.ShareModel;
import com.rt241.citytourplanner.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TourPlaceDetailAdapter extends RecyclerView.Adapter<TourPlaceDetailAdapter.ViewHolder> {

    private List<Place> placeModelList;
    private Context context;


    public TourPlaceDetailAdapter(List<Place> shareModelList, Context context) {
        this.placeModelList = shareModelList;
        this.context = context;
    }


    @NonNull
    @Override
    public TourPlaceDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tour_place_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Place shareModel = placeModelList.get(position);
        holder.placeName.setText(shareModel.getName());
        Picasso.get().load(shareModel.getPhoto()).into(holder.pic);
    }

    @Override
    public int getItemCount() {
        return placeModelList.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder {
        private TextView placeName;
        private ImageView pic;
//        private LinearLayout addLocation;

        public ViewHolder(View view) {
            super(view);
            placeName = view.findViewById(R.id.placeName);
         //   addLocation = view.findViewById(R.id.addLocation);
            pic = view.findViewById(R.id.pic);
        }
    }
}

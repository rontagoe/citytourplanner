package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rt241.citytourplanner.ItenerayDetailsActivity;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.Models.ShareModel;
import com.rt241.citytourplanner.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TouredPlacesAdapter extends RecyclerView.Adapter<TouredPlacesAdapter.ViewHolder> {

    private List<Place> shareModelList;
    private Context context;


    public TouredPlacesAdapter(List<Place> shareModelList, Context context) {
        this.shareModelList = shareModelList;
        this.context = context;
    }


    @NonNull
    @Override
    public TouredPlacesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.toured_place_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Place shareModel = shareModelList.get(position);

        holder.placeName.setText(shareModel.getName());
    }

    @Override
    public int getItemCount() {
        return shareModelList.size();
    }

    class ViewHolder extends  RecyclerView.ViewHolder {
        private TextView placeName;

        public ViewHolder(View view) {
            super(view);
            placeName = view.findViewById(R.id.placeName);
        }
    }
}

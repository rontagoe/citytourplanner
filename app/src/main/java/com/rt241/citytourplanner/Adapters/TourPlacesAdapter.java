package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.rt241.citytourplanner.LocationDetails;
import com.rt241.citytourplanner.Models.Favorite;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.R;
import com.rt241.citytourplanner.helper.DatabaseHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class TourPlacesAdapter extends RecyclerView.Adapter<TourPlacesAdapter.HomeViewHolder> {


    private List<Place> homeModelList;
    private Context context;

    TourPlacesClickListener clickListener;

    private List<Favorite> favouriteList = new ArrayList<>();
    public static ArrayList<Integer> selectedPlaces = new ArrayList<>();

    private SharedPreferences prefs;
    SharedPreferences.Editor edit;

    private DatabaseHelper db;

    public TourPlacesAdapter(List<Place> homeModelList, Context context) {
        this.homeModelList = homeModelList;
        this.context = context;
        notifyDataSetChanged();

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        edit = prefs.edit();

        db = new DatabaseHelper(context);
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tour_place_list_item_row, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeViewHolder holder, final int position) {

        Favorite favourite = null;
        final Place homeModel = homeModelList.get(position);

       /* if (db.isFavourite(String.valueOf(homeModel.getId()))) {
            holder.imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_24px));
        } else {
            holder.imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_outline));
        }*/

        Picasso.get().load(homeModel.getPhoto()).into(holder.imagePlace);
        holder.place.setText(homeModel.getCity() == null ? "-" : homeModel.getCity().getName() + ", " + homeModel.getCity().getCountry());
        holder.city.setText(homeModel.getName().isEmpty() || homeModel.getName() == null ? "-" : homeModel.getName());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(context, LocationDetails.class);
                intent.putExtra("id",String.valueOf(homeModel.getId()));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);*/
                //if(selectedPlaces != null){
                    if (!selectedPlaces.contains(homeModel.getId())) {
                        selectedPlaces.add(homeModel.getId());
                        holder.addToItinerary.setText("");
                        holder.addToItinerary.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                    } else {
                        selectedPlaces.remove((Integer) homeModel.getId());
                        holder.addToItinerary.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                        holder.addToItinerary.setText("");
                    }
              //  }
            }
        });
        /*holder.imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (db.isFavourite(String.valueOf(homeModel.getId()))) {
                    deleteFavorite(homeModel,position);
                } else {
                    postFavorite(homeModel,position);
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return homeModelList.size();
    }

    public void setClickListener(TourPlacesClickListener clickListener) {
        this.clickListener = clickListener;
    }

    class HomeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imagePlace;
        private ImageView imgFav;
        private TextView place;
        private TextView city;
        private TextView addToItinerary;
        private CardView cardView;

        public HomeViewHolder(View view) {
            super(view);
            imagePlace = view.findViewById(R.id.imagePlace);
            imgFav = view.findViewById(R.id.fav);
            place = view.findViewById(R.id.country);
            addToItinerary = view.findViewById(R.id.addToItinerary);
            cardView = view.findViewById(R.id.cardView);
            city = view.findViewById(R.id.city);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null) {
                clickListener.onClick(view, getAdapterPosition());
            }
        }
    }

    public interface TourPlacesClickListener {
        void onClick(View view, int position);
    }
}

package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.EnterDaysActivity;
import com.rt241.citytourplanner.LocationDetails;
import com.rt241.citytourplanner.Models.City;
import com.rt241.citytourplanner.Models.Favorite;
import com.rt241.citytourplanner.Models.HomeModel;
import com.rt241.citytourplanner.R;
import com.rt241.citytourplanner.Utils.Constants;
import com.rt241.citytourplanner.helper.DatabaseHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {


    private List<City> homeModelList;
    private Context context;

    private List<Favorite> favouriteList = new ArrayList<>();

    private SharedPreferences prefs;
    SharedPreferences.Editor edit;

    private DatabaseHelper db;

    public HomeAdapter(List<City> homeModelList, Context context) {
        this.homeModelList = homeModelList;
        this.context = context;
        notifyDataSetChanged();

        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        edit = prefs.edit();

        db = new DatabaseHelper(context);
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_list_row, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HomeViewHolder holder, final int position) {

        Favorite favourite = null;
        final City homeModel = homeModelList.get(position);

        if (db.isFavourite(String.valueOf(homeModel.getId()))) {
            holder.imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_24px));
        } else {
            holder.imgFav.setImageDrawable(context.getResources().getDrawable(R.drawable.favorite_outline));
        }

        Picasso.get().load(homeModel.getPhoto()).fit().into(holder.imagePlace);
        holder.city.setText(homeModel.getName());
        holder.country.setText(homeModel.getCountry());
        holder.addToItinerary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EnterDaysActivity.class);
                intent.putExtra("id",String.valueOf(homeModel.getId()));
                intent.putExtra("placeName",homeModel.getName());
                context.startActivity(intent);
            }
        });
        /*holder.imgFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (db.isFavourite(String.valueOf(homeModel.getId()))) {
                    deleteFavorite(homeModel,position);
                } else {
                    postFavorite(homeModel,position);
                }
            }
        });*/
    }

    private void loadFavourite() {
        favouriteList.clear();
        Cursor cursor = db.getFavourite();
        if (cursor.moveToFirst()) {
            do {
                Favorite n = new Favorite(cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getInt(cursor.getColumnIndex("user_id")),
                        cursor.getInt(cursor.getColumnIndex("place_id")),
                        cursor.getString(cursor.getColumnIndex("place")),
                        cursor.getString(cursor.getColumnIndex("city")));

               // favouriteList.clear();
                favouriteList.add(n);
                notifyDataSetChanged();
            } while (cursor.moveToNext());
        }
        notifyDataSetChanged();
    }

    private void postFavorite(final HomeModel favorite, int position){
        SharedPreferences sharedPreferences = context.getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("access_token", "");
        System.out.println(token+ Constants.FAVOURITE_TOURS+"kdk");
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("tour_id", favorite.getId());
        } catch(JSONException ex) {
            ex.printStackTrace();
        }
        Ion.with(context)
                .load("POST", Constants.FAVOURITE_TOURS)
                .setHeader("Accept","application/json")
                .setHeader("Content-Type","application/json")
                .setHeader("Authorization","Bearer "+token)
                .setStringBody(jsonObject.toString())
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        String jsonResult = result.trim();
                        System.out.println(result+"LOOO");
                        try {
                            JSONObject jsonObject1 = new JSONObject(jsonResult);
                            boolean isSuccess = jsonObject1.getBoolean("success");
                            if(isSuccess){
                                boolean isDone = db.insertFavourite(favorite);
                                if (isDone) {
                                    loadFavourite();
                                    notifyDataSetChanged();
                                }
                            }
                            loadFavourite();
                            notifyDataSetChanged();
                        } catch(JSONException ex) {
                            ex.printStackTrace();
                           // progressDialog.dismiss();
                        }
                        notifyDataSetChanged();
                    }
                });
    }

    private void deleteFavorite(final HomeModel favorite, int position){
        SharedPreferences sharedPreferences = context.getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("access_token", "");
        System.out.println(token+ Constants.FAVOURITE_TOURS+"kdk");
        Ion.with(context)
                .load("DELETE", Constants.DELETE_FAVOURITE_TOURS+"/"+favorite.getId())
                .setHeader("Accept","application/json")
                .setHeader("Content-Type","application/json")
                .setHeader("Authorization","Bearer "+token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        String jsonResult = result.trim();
                        System.out.println(Constants.DELETE_FAVOURITE_TOURS+"/"+favorite.getId());
                        System.out.println(result+"Hi");
                        try {
                            JSONObject jsonObject1 = new JSONObject(jsonResult);
                            boolean isSuccess = jsonObject1.getBoolean("success");
                            if(isSuccess){
                                boolean isDone = db.deleteFavourite(favorite.getId());
                                if (isDone) {
                                    loadFavourite();
                                    notifyDataSetChanged();
                                }
                            }
                            loadFavourite();
                        } catch(JSONException ex) {
                            ex.printStackTrace();
                           // progressDialog.dismiss();
                        }
                    }
                });
    }

    @Override
    public int getItemCount() {
        return homeModelList.size();
    }

    class HomeViewHolder extends RecyclerView.ViewHolder {
        private ImageView imagePlace;
        private ImageView imgFav;
        private TextView country;
        private TextView city;
        private LinearLayout addToItinerary;
        public HomeViewHolder(View view) {
            super(view);
            imagePlace = view.findViewById(R.id.imagePlace);
            imgFav = view.findViewById(R.id.fav);
            country = view.findViewById(R.id.country);
            addToItinerary = view.findViewById(R.id.addToItinerary);
            city = view.findViewById(R.id.city);
        }
    }
}

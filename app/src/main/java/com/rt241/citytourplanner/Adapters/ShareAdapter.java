package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.rt241.citytourplanner.ItenerayDetailsActivity;
import com.rt241.citytourplanner.Models.ShareModel;
import com.rt241.citytourplanner.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ShareAdapter extends RecyclerView.Adapter<ShareAdapter.ViewHolder> {

    private List<ShareModel> shareModelList;
    private Context context;


    public ShareAdapter(List<ShareModel> shareModelList, Context context) {
        this.shareModelList = shareModelList;
        this.context = context;
    }


    @NonNull
    @Override
    public ShareAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itinerary_list_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ShareModel shareModel = shareModelList.get(position);
        /*Log.d("CITY:::::",shareModel.getCity().);
        Log.d("CITY:::::",shareModel.getCity_id());
        Log.d("CITY:::::",shareModel.getPlace());
        Log.d("CITY:::::",String.valueOf(shareModel.getId()));*/

        SimpleDateFormat format;
        String startDate =null;
        String endDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


        try {

            Date s_date = sdf.parse(shareModel.getStartDate());
            Date e_date = sdf.parse(shareModel.getEndDate());

            format = new SimpleDateFormat("dd-MMM-yyyy");

              startDate = format.format(s_date);
              endDate = format.format(e_date);


            long diff =  e_date.getTime() - s_date.getTime();




            holder.date.setText(String.format("%s - %s", startDate,endDate));
            holder.days.setText(String.format("%s day stay", TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)));
        } catch (ParseException e) {
            e.printStackTrace();
        }






        holder.placeName.setText(shareModel.getCity().getName());

        Picasso.get().load(shareModel.getCity().getPhoto()).into(holder.pic);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ItenerayDetailsActivity.class);
              //  Intent i = new Intent(context, AddPlaceToTourActivity.class);
                i.putExtra("tour_id", String.valueOf(shareModel.getId()));
                i.putExtra("city_id", String.valueOf(shareModel.getCity().getId()));
                context.startActivity(i);
            }
        });

        if(shareModel.getPlacesList() == null){
            holder.linearLayout.setVisibility(View.VISIBLE);
            holder.recyclerView.setVisibility(View.GONE);
        }else{
            holder.linearLayout.setVisibility(View.GONE);
            holder.recyclerView.setVisibility(View.VISIBLE);

            TouredPlacesAdapter homeAdapter = new TouredPlacesAdapter(shareModel.getPlacesList(), context);
            holder.recyclerView.setAdapter(homeAdapter);
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        }
       // holder.pic.setImageResource(shareModel.getPic());
    }

    @Override
    public int getItemCount() {
        return shareModelList.size();
    }

    static class ViewHolder extends  RecyclerView.ViewHolder {
        private TextView placeName;
        private TextView date,days;
        private ImageView pic;
        private LinearLayout addLocation;
        private LinearLayout linearLayout;
        private RecyclerView recyclerView;
        private CardView cardView;

        public ViewHolder(View view) {
            super(view);
            cardView = view.findViewById(R.id.cardView);
            date = view.findViewById(R.id.date);
            days = view.findViewById(R.id.days);
            recyclerView = view.findViewById(R.id.touredPlaces);
            placeName = view.findViewById(R.id.placeName);
            addLocation = view.findViewById(R.id.addLocation);
            linearLayout = view.findViewById(R.id.linearLayout);
            pic = view.findViewById(R.id.pic);
        }
    }
}

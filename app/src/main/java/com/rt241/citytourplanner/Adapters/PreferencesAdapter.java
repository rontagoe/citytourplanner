package com.rt241.citytourplanner.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.rt241.citytourplanner.Models.PreferencesModel;
import com.rt241.citytourplanner.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


public class PreferencesAdapter extends RecyclerView.Adapter<PreferencesAdapter.PreferencesViewHolder> implements Filterable {

    private List<PreferencesModel> preferencesModelList;
    private List<PreferencesModel> preferencesModelListFiltered;
    private PreferenceAdapterListener preferenceAdapterListener;
    private Context context;

    private Vector<Integer> selectedPosition = new Vector<Integer>();

    public static ArrayList<Integer> selectedPreference = new ArrayList<>();

    public PreferencesAdapter(List<PreferencesModel> preferencesModelList, Context context) {
        this.preferencesModelList = preferencesModelList;
        this.preferencesModelListFiltered = preferencesModelList;
        this.context = context;
        this.preferenceAdapterListener = preferenceAdapterListener;
    }

    @NonNull
    @Override
    public PreferencesAdapter.PreferencesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.preferences_list_row, parent, false);
        final PreferencesViewHolder viewHolder =   new PreferencesViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PreferencesModel preferencesModel = preferencesModelListFiltered.get(viewHolder.getAdapterPosition());

//                //here you set your current position from holder of clicked view
                 if(!selectedPreference.contains(preferencesModel.getId())) {
                     selectedPreference.add(preferencesModel.getId());
                 } else {
                     selectedPreference.remove((Integer) preferencesModel.getId());
                 }
//                //here you inform view that something was change - view will be invalidated
                notifyDataSetChanged();
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PreferencesAdapter.PreferencesViewHolder holder, int position) {
        PreferencesModel preferencesModel = preferencesModelListFiltered.get(position);

       if (selectedPreference.contains(preferencesModel.getId())) {
          holder.check.setVisibility(View.VISIBLE);
       } else {
           holder.check.setVisibility(View.GONE);
       }
        holder.preferences.setText(preferencesModel.getPreference());
    }



    @Override
    public int getItemCount() {
        return preferencesModelListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    preferencesModelListFiltered = preferencesModelList;
                } else {
                    List<PreferencesModel> filteredList = new ArrayList<>();
                    for (PreferencesModel preferencesModel : preferencesModelList) {
                        if (preferencesModel.getPreference().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(preferencesModel);
                        }
                    }
                    preferencesModelListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = preferencesModelListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                preferencesModelListFiltered = (ArrayList<PreferencesModel>) filterResults.values;

                notifyDataSetChanged();
            }
        };
    }

    class PreferencesViewHolder extends RecyclerView.ViewHolder {
        private TextView preferences;
        private ImageView check;
        public PreferencesViewHolder(View view) {
            super(view);
            preferences = view.findViewById(R.id.preferences);
            check = view.findViewById(R.id.check);
            view.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    System.out.println("Black starlets");
                    preferenceAdapterListener.onPreferenceSelected(preferencesModelListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface PreferenceAdapterListener {
        void onPreferenceSelected(PreferencesModel preferencesModel);
    }
}

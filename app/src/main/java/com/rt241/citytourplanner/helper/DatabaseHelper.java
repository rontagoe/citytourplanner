package com.rt241.citytourplanner.helper;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


import com.rt241.citytourplanner.Models.Favorite;
import com.rt241.citytourplanner.Models.HomeModel;

import org.json.JSONException;
import org.json.JSONObject;

public class DatabaseHelper extends SQLiteOpenHelper {

    //Constants for Database name, table name, and column names
    public static final String DB_NAME = "citytourplanner_db";
    public static final String CATEGORIES_TABLE = "categories";
    public static final String DISCOUNT_TABLE = "discount_coupons";
    public static final String VENDORS_TABLE = "vendors";
    public static final String FAVOURITES_TABLE = "favourites";
    public static final String NOTIFICATIONS_TABLE = "notifications";
    public static final String LEGALS_TABLE = "legals";


    public static final String COLUMN_ID = "id";
    public static final String cat_name = "cat_name";
    public static final String img_url = "img_url";
    public static final String status = "status";

    public static final String category_id = "category_id";
    public static final String discount_code = "discount_code";
    public static final String discount_label = "discount_label";
    public static final String discount_value = "discount_value";
    public static final String description = "description";
    public static final String featured = "featured";
    public static final String popular = "popular";
    public static final String hot = "hot";
    public static final String created_at = "created_at";
    public static final String banner_url = "banner_url";
    public static final String vendor_name = "vendor_name";
    public static final String vendor_id = "vendor_id";
    public static final String website_link = "website_link";

    public static final String name = "name";
    public static final String identifier = "identifier";
    public static final String key = "_key";
    public static final String updated_at = "updated_at";


    // public static final String vendor_name= "vendor_name";
    public static final String vendor_desc = "vendor_desc";
    public static final String mobile_number_one = "mobile_number_one";
    public static final String logo_url = "logo_url";
    public static final String email = "email";
    public static final String location = "location";
    public static final String work_hours = "work_hours";

    public static final String notificationID = "notification_id";
    public static final String title = "title";
    public static final String body = "body";
    //public static final String category_id = "category_id";

    /*public static final String discount_label = "discount_label";
    public static final String discount_value = "discount_value";
    public static final String description = "description";
    public static final String featured = "featured";
    public static final String popular = "popular";
    public static final String created_at = "created_at";
    public static final String banner_url = "banner_url";
    public static final String vendor_name = "vendor_name";
    public static final String vendor_id = "vendor_id";
    public static final String website_link = "website_link";*/

    //database version
    private static final int DB_VERSION = 3;

    //Constructor
    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    //creating the database
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE " + CATEGORIES_TABLE
                + "(" + COLUMN_ID + " INTEGER, "
                + cat_name + " VARCHAR, "
                + img_url + " VARCHAR, "
                + status + " TINYINT);";

        String sql1 = "CREATE TABLE " + DISCOUNT_TABLE
                + "(" + COLUMN_ID + " INTEGER, "
                + category_id + " INTEGER, "
                + discount_code + " VARCHAR, "
                + discount_label + " VARCHAR, "
                + discount_value + " VARCHAR, "
                + description + " VARCHAR, "
                + featured + " TINYINT, "
                + popular + " TINYINT, "
                + hot + " TINYINT, "
                + cat_name + " VARCHAR, "
                + created_at + " VARCHAR, "
                + banner_url + " VARCHAR, "
                + img_url + " VARCHAR, "
                + vendor_name + " VARCHAR, "
                + vendor_id + " INTEGER, "
                + website_link + " VARCHAR, "
                + status + " TINYINT);";

        String sql2 = "CREATE TABLE " + LEGALS_TABLE
                + "(" + COLUMN_ID + " INTEGER, "
                + name + " VARCHAR, "
                + description + " VARCHAR, "
                + key + " VARCHAR, "
                + identifier + " VARCHAR, "
                + created_at + " VARCHAR, "
                + updated_at + " VARCHAR, "
                + status + " TINYINT);";

        String sql5 = "CREATE TABLE " + NOTIFICATIONS_TABLE
                + "(" + COLUMN_ID + " INTEGER PRIMARY KEY, "
                + notificationID + " TEXT, "
                + title + " VARCHAR, "
                + body + " VARCHAR, "
                + status + " TINYINT);";

        String sql3 = "CREATE TABLE " + VENDORS_TABLE
                + "(" + COLUMN_ID + " INTEGER, "
                + vendor_name + " VARCHAR, "
                + vendor_desc + " VARCHAR, "
                + mobile_number_one + " VARCHAR, "
                + website_link + " VARCHAR, "
                + logo_url + " VARCHAR, "
                + email + " VARCHAR, "
                + location + " VARCHAR, "
                + work_hours + " VARCHAR, "
                + category_id + " INTEGER, "
                + status + " TINYINT);";

        String sql4 = "CREATE TABLE IF NOT EXISTS " + FAVOURITES_TABLE
                + "(" + "id INTEGER, "
                + "place VARCHAR, "
                + "city VARCHAR, "
                + "user_id INTEGER, "
                +  "place_id INTEGER, "
                + "img_url VARCHAR, "
                + "status TINYINT);";

        db.execSQL(sql);
        db.execSQL(sql1);
        db.execSQL(sql2);
        db.execSQL(sql4);
        db.execSQL(sql3);
        db.execSQL(sql5);
        // db.close();
    }

    //upgrading the database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + CATEGORIES_TABLE;
        String sql1 = "DROP TABLE IF EXISTS " + LEGALS_TABLE;
        String sql2 = "DROP TABLE IF EXISTS " + DISCOUNT_TABLE;
        String sql3 = "DROP TABLE IF EXISTS " + VENDORS_TABLE;
        String sql4 = "DROP TABLE IF EXISTS " + FAVOURITES_TABLE;
        String sql5 = "DROP TABLE IF EXISTS " + NOTIFICATIONS_TABLE;
        db.execSQL(sql);
        db.execSQL(sql1);
        db.execSQL(sql2);
        db.execSQL(sql4);
        db.execSQL(sql3);
        db.execSQL(sql5);
        onCreate(db);

    }

    public boolean insertFavourite(HomeModel jObject) {
        Log.d("SRUFF::::", jObject.toString());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //  try {
/*            JSONObject jsonObject = new JSONObject(jObject);

            Log.d("SRUFF::::",jsonObject.toString());
            int _vendor_id = jsonObject.getInt("id");
            String _vendor_name = jsonObject.getString("vendor_name");
            String _vendor_desc = jsonObject.getString("vendor_desc");
            String _mobile_number_one = jsonObject.getString("mobile_number_one");
            String _logo_url = jsonObject.getString("logo_url");
            String _website_link = jsonObject.getString("website_link");
            String _email = jsonObject.getString("email");
            String _location = jsonObject.getString("location");
            String _work_hours = jsonObject.getString("work_hours");
            int _category_id = jsonObject.getInt("category_id");
            boolean _status = jsonObject.getBoolean("status");*/

        Cursor c = db.rawQuery("SELECT * FROM favourites WHERE id=?", new String[]{String.valueOf(jObject.getId())});

        // Log("Cursor Count : " + c.getCount());
        if (c.getCount() > 0) {
            Log.d("EXISTS", "YES OOO");
            deleteFavourite(jObject.getId());

        } else {
            Log.d("APUUUUU", "TOOOOOOOOOOOORRR");

            contentValues.put(COLUMN_ID, jObject.getId());
            contentValues.put("city", jObject.getCity());
            contentValues.put("place", jObject.getPlace());
            contentValues.put("img_url", "http://i.imgur.com/DvpvklR.png");
            contentValues.put("place_id", jObject.getPlace_id());
         //   contentValues.put("user_id", jObject.getUser_id());
            contentValues.put(status, 1);


            Log.d("USER::::", String.valueOf(contentValues));

            db.insert(FAVOURITES_TABLE, null, contentValues);
        }


        db.close();
        /*} catch (JSONException e) {
            e.printStackTrace();
            return false;
        }*/
        return true;
    }

    public boolean isFavourite(String itemId){
        SQLiteDatabase database = this.getReadableDatabase();

        String query = String.format("SELECT * FROM favourites WHERE id='%s';",itemId);
        Cursor cursor = database.rawQuery(query,null);
        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }

    public boolean deleteFavourite(int _id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM " + FAVOURITES_TABLE + " WHERE " + "id" + " = " + _id + ";";
        db.execSQL(sql);
        db.close();
        return true;
    }

    public boolean deleteNotification(String _id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM " + NOTIFICATIONS_TABLE + " WHERE " + COLUMN_ID + " = '" + _id + "';";
        db.execSQL(sql);
        db.close();
        return true;
    }

    public boolean deleteDiscount() {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM " + DISCOUNT_TABLE + ";";
        db.execSQL(sql);
        return true;
    }

    public Cursor getFavourite() {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + FAVOURITES_TABLE + " ORDER BY " + COLUMN_ID + " DESC;";
        Cursor c = db.rawQuery(sql, null);
        return c;
    }

}

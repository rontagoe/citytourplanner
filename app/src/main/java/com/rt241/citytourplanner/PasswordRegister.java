package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class PasswordRegister extends AppCompatActivity {
    EditText passwordEdt;
    TextView next;
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_register);
        sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        passwordEdt = findViewById(R.id.password);
        next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = passwordEdt.getText().toString();


                if(password.isEmpty() || password.length() < 8){
                    passwordEdt.setError("Password should contain at least 8 characters");
                }else{
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("password", password);
                    editor.apply();
                    Intent i = new Intent(PasswordRegister.this, UsernameActivity.class);
                    startActivity(i);
                }
            }
        });
    }
}
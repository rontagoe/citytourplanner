package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.rt241.citytourplanner.Adapters.HomeAdapter;
import com.rt241.citytourplanner.Models.HomeModel;
import com.rt241.citytourplanner.Utils.Constants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.github.ponnamkarthik.richlinkpreview.RichLinkView;
import io.github.ponnamkarthik.richlinkpreview.ViewListener;

public class LocationDetails extends AppCompatActivity {

    TextView placeTitle, placeDescription, ratingTxt, placeReviews;
    ProgressDialog progressDialog;
    ImageView bgImage, viewInMap, addToReminder,back;
    FloatingActionButton fabAddLocationToPlace;
    SharedPreferences sharedPreferences;
    String token, placeName, tour_id;
    String id,yt_link;
    RatingBar ratingBar;
    double lat, lng;
    RichLinkView richLinkView;
    TextView linear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_details);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            id = extras.getString("id");
            //The key argument here must match that used in the other activity
        }

        sharedPreferences = getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("access_token", "");
        tour_id = sharedPreferences.getString("tour_id", "");
        richLinkView = findViewById(R.id.richLinkView);
        linear = findViewById(R.id.textView4);
        progressDialog = new ProgressDialog(this);

        placeTitle = findViewById(R.id.placeTitle);
        placeReviews = findViewById(R.id.reviews);
        bgImage = findViewById(R.id.bgImage);
        back = findViewById(R.id.back);
        viewInMap = findViewById(R.id.viewInMap);
        addToReminder = findViewById(R.id.addToReminder);
        fabAddLocationToPlace = findViewById(R.id.fabAddLocationToPlace);
        placeDescription = findViewById(R.id.placeDescription);
        ratingBar = findViewById(R.id.rating);
        ratingTxt = findViewById(R.id.ratingTxt);

        progressDialog.show();
        progressDialog.setMessage("Loading");

     //   bgImage.setColorFilter(Color., PorterDuff.Mode.MULTIPLY);

        back.setOnClickListener(view -> finish());

        viewInMap.setOnClickListener(view -> {
            Uri mapUri = Uri.parse("geo:0,0?q=" + lat + "," + lng + " (" + placeName + ")");
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        });

        linear.setOnClickListener(view -> {
            Intent intent = new  Intent(Intent.ACTION_VIEW);

            intent.setPackage("com.google.android.youtube");
            intent.setData(Uri.parse(yt_link));
           // intent.getLaunchIntentForPackage("com.google.android.youtube");
            startActivity(intent);
        });

        addToReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", cal.getTimeInMillis());
                intent.putExtra("allDay", true);
                intent.putExtra("rrule", "FREQ=YEARLY");
                intent.putExtra("endTime", cal.getTimeInMillis() + 60 * 60 * 1000);
                intent.putExtra("title", "A visit to "+placeName);
                startActivity(intent);
            }
        });

        fabAddLocationToPlace.setOnClickListener(view -> addPlaceToTour());

        getLocationDetails();
    }

    private void addPlaceToTour() {
        JsonObject json = new JsonObject();
        //json.addProperty("foo", "bar");
        Ion.with(this)
                .load("POST", Constants.CREATE_TOUR + "/" + tour_id+"/places/"+id)
                .setLogging("AddToPlace", Log.DEBUG)
                .setHeader("Accept", "application/json")
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", "Bearer " + token)
                .setJsonObjectBody(json)
                .asJsonObject()
                .withResponse()
                .setCallback(new FutureCallback<Response<JsonObject>>() {
                    @Override
                    public void onCompleted(Exception e, Response<JsonObject> result) {

                        int responseCode = result.getHeaders().code();
                        if(responseCode == 200) {
                            finish();
                            Intent i = new Intent(LocationDetails.this, DashboardActivity.class);
                            startActivity(i);
                        } else {
                            Toast.makeText(LocationDetails.this, "Sorry something went wrong. Please try again", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();
                    }
                });
    }

    private void getLocationDetails() {
        Ion.with(this)
                .load("GET", Constants.PLACE_DETAILS + "/" + id)
                .setHeader("Accept", "application/json")
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", "Bearer " + token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        String jsonResult = result.trim();
                        System.out.println(result + "Hi");
                        try {
                            List<HomeModel> homeModelList = new ArrayList<>();
                            JSONObject jsonObject1 = new JSONObject(jsonResult);
                            JSONArray jsonArray = jsonObject1.getJSONArray("data");
                            //  for(int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.getJSONObject(0);
                            int id = jsonObject2.getInt("id");
                            String name = jsonObject2.getString("name");
                            placeName = name;
                            String description = jsonObject2.getString("description");
                            String photo = jsonObject2.getString("photo");
                            double ratings = jsonObject2.getDouble("rating");
                            lat = jsonObject2.getDouble("lat");
                            lng = jsonObject2.getDouble("lng");
                            String reviews = jsonObject2.getString("reviews");
                            String city = jsonObject2.getString("city");
                            String place_youtube_links = jsonObject2.getString("youtube_link");
                            yt_link = place_youtube_links;

                            JSONObject jsonObject3 = new JSONObject(city);
                            String cId = jsonObject3.getString("id");
                          //  String name = jsonObject3.getString("name");
                            String country = jsonObject3.getString("country");
                            String city_photo = jsonObject3.getString("photo");
                            String youtube_links = jsonObject3.getString("youtube_links");

                            placeTitle.setText(name.isEmpty() ? "-" : name);
                            placeReviews.setText(reviews.isEmpty() ? "-" : reviews);
                            ratingBar.setRating(Float.parseFloat(String.valueOf(ratings)));
                            ratingTxt.setText(String.valueOf(ratings));
                            placeDescription.setText(description.isEmpty() ? "-" : description);
                            Picasso.get().load(photo).into(bgImage);

                            richLinkView.setLink(place_youtube_links, new ViewListener() {

                                @Override
                                public void onSuccess(boolean status) {

                                }

                                @Override
                                public void onError(Exception e) {

                                }
                            });
                            //   }
                            progressDialog.dismiss();
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                });
    }
}
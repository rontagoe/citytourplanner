package com.rt241.citytourplanner.Utils;

public class Constants {

  //  public static final String BASE_URL = "http://139.162.255.68/api/";
    public static final String BASE_URL = "https://citytour.rontagoe.com/api/";
    public static final String REGISTER = BASE_URL+"auth/register";
    public static final String LOGIN = BASE_URL+"auth/login";
    public static final String TAGS = BASE_URL+"data/tags";
    public static final String GET_USER_TOURS = BASE_URL + "tours";
    public static final String FAVOURITE_TOURS = BASE_URL + "tours/places/favorite";
    public static final String DELETE_FAVOURITE_TOURS = BASE_URL + "data/places/favorites";
    public static final String DELETE_USER_TOURS = BASE_URL + "tours";
    public static final String SEARCH_CITY = BASE_URL + "data/cities?search=";
    public static final String CREATE_TOUR = BASE_URL + "tours";
    public static final String PLACE_DETAILS = BASE_URL + "data/places/id";
    public static final String PLACE = BASE_URL + "data/places?city_id=";
    public static final String CITY = BASE_URL + "data/cities";
    public static final String USER = BASE_URL + "data/user/profile";
    public static final String RECOMMENDED_PLACES = BASE_URL + "data/recommended-places";
    public static final String PICTURE_UPLOAD = BASE_URL + "auth/add-picture";
    public static final String PREFERENCE= BASE_URL + "auth/tags/update";
}

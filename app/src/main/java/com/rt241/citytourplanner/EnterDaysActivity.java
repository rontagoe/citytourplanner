package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class EnterDaysActivity extends AppCompatActivity {
    private TextView days_in_city;
    private FrameLayout frameLayout;
    private EditText days;
    private String cityId;
    private String city;
    private ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_days);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            cityId = extras.getString("id");
            city = extras.getString("placeName");
            //The key argument here must match that used in the other activity
        }

        frameLayout = findViewById(R.id.frameLayout);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               doDays();
            }
        });
        days_in_city = findViewById(R.id.days_in_city);
        days = findViewById(R.id.days);
        back = findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        days_in_city.setText(String.format("How many days are you going to be in %s ?", city));
    }

    private void doDays() {
        if(!days.getText().toString().trim().isEmpty()) {
            Intent i = new Intent(EnterDaysActivity.this, TourPlacesActivity.class);
        //    Intent i = new Intent(EnterDaysActivity.this, ItineraryCalendarActivity.class);
            i.putExtra("days", days.getText().toString());
            i.putExtra("cityId", cityId);
            i.putExtra("cityName", city);
            startActivity(i);
        }else{
            Toast.makeText(this,"Enter number of days",Toast.LENGTH_LONG).show();
        }
    }
}
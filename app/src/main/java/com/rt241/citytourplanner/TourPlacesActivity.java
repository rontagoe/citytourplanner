package com.rt241.citytourplanner;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.TourPlacesAdapter;
import com.rt241.citytourplanner.Models.City;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.rt241.citytourplanner.Adapters.TourPlacesAdapter.selectedPlaces;

public class TourPlacesActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    SharedPreferences sharedPreferences;
    String token,cityId,days,cityName;
    TextView textView;
    FloatingActionButton next;
    List<Place> homeModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_places);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            cityId = extras.getString("cityId");
            days = extras.getString("days");
            cityName = extras.getString("cityName");
            //selectedPlaces = extras.getIntegerArrayList("selectedPlaces");
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        sharedPreferences = this.getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("access_token", "");

        recyclerView = findViewById(R.id.recyclerview);
        textView = findViewById(R.id.textView2);
        next = findViewById(R.id.next);

        textView.setText("");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(selectedPlaces.size() == 0){
                    Toast.makeText(TourPlacesActivity.this, "Kindly select a place", Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent = new Intent(TourPlacesActivity.this, ItineraryCalendarActivity.class);
                    intent.putExtra("id", cityId);
                    intent.putIntegerArrayListExtra("selectedPlaces", selectedPlaces);
                    intent.putExtra("days", days);
                    intent.putExtra("cityId", cityId);
                    intent.putExtra("cityName", cityName);
                    //   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }

          //      selectedPlaces
            }
        });

        getTourPlaces();
    }

    private void getTourPlaces() {
        Ion.with(this)
                .load("GET", Constants.PLACE+cityId)
                .setLogging("TOUR PLACE", Log.DEBUG)
                .setHeader("Accept","application/json")
                .setHeader("Content-Type","application/json")
                .setHeader("Authorization","Bearer "+token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        String jsonResult = result.trim();

                        try {
                            homeModelList = new ArrayList<>();
                            JSONObject jsonObject1 = new JSONObject(jsonResult);
                            JSONArray jsonArray = jsonObject1.getJSONArray("data");
                            if(jsonArray.length() == 0){
                                textView.setText("No ---records");
                            }else {
                                textView.setText("");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    System.out.println(jsonObject2.toString() + "ododod");
                                    int id = jsonObject2.getInt("id");
                                    String name = jsonObject2.getString("name");
                                    String description = jsonObject2.getString("description");
                                    double lat = jsonObject2.getDouble("lat");
                                    double lng = jsonObject2.getDouble("lng");
                                    double rating = jsonObject2.getDouble("rating");
                                    //. null ? 0.00 : jsonObject2.getDouble("rating");
                                    String updated_at = jsonObject2.getString("updated_at");
                                    String reviews = jsonObject2.getString("reviews");
                                    String photo = jsonObject2.getString("photo");
                                    Log.e("photo", photo);
                                    JSONObject city = jsonObject2.getJSONObject("city");
                                    int cityId = city.getInt("id");
                                    String cityName = city.getString("name");
                                    String cityCountry = city.getString("country");

                                    homeModelList.add(new Place(id, name, description, lat, lng, new City(cityId, cityName, cityCountry,"-"), updated_at, rating, reviews, !photo.isEmpty() ? photo : "http://i.imgur.com/DvpvklR.png"));
                                }
                                TourPlacesAdapter homeAdapter = new TourPlacesAdapter(homeModelList, getApplicationContext());
                                recyclerView.setAdapter(homeAdapter);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                              //  homeAdapter.setClickListener(TourPlacesActivity.this);
                            }
                            progressDialog.dismiss();
                        } catch(JSONException ex) {
                            textView.setText(ex.getMessage());
                            ex.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                });
    }

   /* @Override
    public void onClick(View view, int position) {
        final Place place = homeModelList.get(position);


    }*/
}
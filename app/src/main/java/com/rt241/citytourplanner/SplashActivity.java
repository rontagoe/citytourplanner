package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;


public class SplashActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        String accessToken = sharedPreferences.getString("access_token", "");
        boolean logged_in = sharedPreferences.getBoolean("logged_in", false);
       if (!logged_in) {
            Intent mainIntent = new Intent(SplashActivity.this,SelectActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();
        } else {
            Intent mainIntent = new Intent(SplashActivity.this,DashboardActivity.class);
            SplashActivity.this.startActivity(mainIntent);
            SplashActivity.this.finish();
        }
    }
}
package com.rt241.citytourplanner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Utils.Constants;
import com.savvi.rangedatepicker.CalendarPickerView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class ItineraryCalendarActivity extends AppCompatActivity {
    CalendarView calendarPickerView;
    FrameLayout frameLayout;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    long date;
    int day = 0,_month=0,_year=0;
    String cityId,city,days;
    ImageView back;
    ArrayList<Integer> selectedPlaces;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itinerary_calendar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
        //    id = extras.getInt("id");
            cityId = extras.getString("cityId");
            city = extras.getString("cityName");
            days = extras.getString("days");
            selectedPlaces = extras.getIntegerArrayList("selectedPlaces");
        }


        progressDialog = new ProgressDialog(this);
        calendarPickerView = findViewById(R.id.calendarView);
        back = findViewById(R.id.back);
        date = calendarPickerView.getDate();


        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        calendarPickerView.setOnDateChangeListener((view, year, month, dayOfMonth) -> {
            _year = year;
            _month = month;
            day = dayOfMonth;
        });
        frameLayout = findViewById(R.id.frameLayout);
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                progressDialog.setMessage("Loading....");

                    Calendar calendar = Calendar.getInstance();
                if (_year == 0) {
                    calendar.setTime(Calendar.getInstance().getTime());
                } else {
                    calendar.set(_year, _month, day);
                }
                    //calendar.set(_year,_month,day);
                    calendar.add(Calendar.DATE, 0);
                    int Year = calendar.get(Calendar.YEAR);
                    int Month = calendar.get(Calendar.MONTH);
                    int Day = calendar.get(Calendar.DAY_OF_MONTH);

                    Calendar calendar2 = Calendar.getInstance();
                if (_year == 0) {
                    calendar2.setTime(Calendar.getInstance().getTime());
                } else {
                    calendar2.set(_year, _month, day);
                }
                calendar2.add(Calendar.DAY_OF_YEAR, Integer.parseInt(days));
                    int Year2 = calendar2.get(Calendar.YEAR);
                    int Month2 = calendar2.get(Calendar.MONTH);
                    int Day2 = calendar2.get(Calendar.DAY_OF_MONTH);



                    String startDate =Year+"-"+(Month+1)+"-"+Day;
                    String endDate =Year2+"-"+(Month2+1)+"-"+Day2;



                 final JSONObject jsonObject = new JSONObject();


                try {
                    jsonObject.put("start_date", startDate);
                    jsonObject.put("end_date", endDate);
                    sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
                    String placeID = sharedPreferences.getString("place_id","");
                    jsonObject.put("city_id", cityId);
                    JSONArray places = new JSONArray(selectedPlaces);
                    jsonObject.put("places", places);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Ion.with(ItineraryCalendarActivity.this)
                        .load("POST",Constants.CREATE_TOUR)
                        .setHeader("Accept","application/json")
                        .setHeader("Content-Type","application/json")
                        .setHeader("Authorization","Bearer "+sharedPreferences.getString("access_token",""))
                        .setStringBody(jsonObject.toString())
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                progressDialog.dismiss();
                                String jsonResult = result.trim();

                                Log.d("HI", String.valueOf(result));
                                try {
                                    JSONObject jsonObject1 = new JSONObject(jsonResult);
                                    if(jsonObject1.has("user_id")) {
                                        Intent i = new Intent(ItineraryCalendarActivity.this, DashboardActivity.class);
                                        startActivity(i);

                                        Toast.makeText(ItineraryCalendarActivity.this, "Success", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }

                            }
                        });
            }
        });
    }
}
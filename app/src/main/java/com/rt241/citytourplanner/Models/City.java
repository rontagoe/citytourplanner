package com.rt241.citytourplanner.Models;

public class City {
    private int id;
    private String name;
    private String country;
    private String photo;

    public City(int id, String name, String country, String photo) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}

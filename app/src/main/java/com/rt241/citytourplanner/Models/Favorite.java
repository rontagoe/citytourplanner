package com.rt241.citytourplanner.Models;

public class Favorite {
    private int id;
    private int user_id;
    private int place_id;
    private String place;
    private String city;

    public Favorite(int id, int user_id, int place_id, String place, String city) {
        this.id = id;
        this.user_id = user_id;
        this.place_id = place_id;
        this.place = place;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getPlace_id() {
        return place_id;
    }

    public void setPlace_id(int place_id) {
        this.place_id = place_id;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}

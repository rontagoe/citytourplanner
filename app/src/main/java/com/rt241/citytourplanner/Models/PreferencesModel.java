package com.rt241.citytourplanner.Models;

public class PreferencesModel {
    private String preference;
    private int id;

    public PreferencesModel(String preference, int id) {
        this.preference = preference;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }
}

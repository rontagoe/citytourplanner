package com.rt241.citytourplanner.Models;

public class PreviousTourModel {
    private String picture;
    private String date;
    private String city;
    private City cityModel;

    public PreviousTourModel(String picture, String date, String city, City cityModel) {
        this.picture = picture;
        this.date = date;
        this.city = city;
        this.cityModel = cityModel;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public City getCityModel() {
        return cityModel;
    }

    public void setCityModel(City cityModel) {
        this.cityModel = cityModel;
    }
}

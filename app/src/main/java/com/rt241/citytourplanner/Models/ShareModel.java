package com.rt241.citytourplanner.Models;

import java.util.List;

public class ShareModel {
    private int id;
    private int place_id;
    private String imagePlace;
    private String place;
    private City city;
    private String city_id;
    private List<Place> placesList;
    private String startDate;
    private String endDate;

    public ShareModel(int id, int place_id, String imagePlace, String place, City city, String city_id, List<Place> placesList, String startDate, String endDate) {
        this.id = id;
        this.place_id = place_id;
        this.imagePlace = imagePlace;
        this.place = place;
        this.city = city;
        this.city_id = city_id;
        this.placesList = placesList;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlace_id() {
        return place_id;
    }

    public void setPlace_id(int place_id) {
        this.place_id = place_id;
    }

    public String getImagePlace() {
        return imagePlace;
    }

    public void setImagePlace(String imagePlace) {
        this.imagePlace = imagePlace;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public List<Place> getPlacesList() {
        return placesList;
    }

    public void setPlacesList(List<Place> placesList) {
        this.placesList = placesList;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}

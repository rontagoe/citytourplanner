package com.rt241.citytourplanner.Models;

public class HomeModel {
    private int id;
    private int place_id;
    private String imagePlace;
    private String place;
    private String city;

    public HomeModel(int id, int place_id, String imagePlace, String place, String city) {
        this.id = id;
        this.place_id = place_id;
        this.imagePlace = imagePlace;
        this.place = place;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPlace_id() {
        return place_id;
    }

    public void setPlace_id(int place_id) {
        this.place_id = place_id;
    }

    public String getImagePlace() {
        return imagePlace;
    }

    public void setImagePlace(String imagePlace) {
        this.imagePlace = imagePlace;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}

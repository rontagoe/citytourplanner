package com.rt241.citytourplanner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    private TextView signIn;
    private ProgressDialog progressDialog;
    private EditText emailAddress;
    private EditText password;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        emailAddress = findViewById(R.id.emailAddress);
        password = findViewById(R.id.password);
        //preferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);

        progressDialog = new ProgressDialog(this);
        signIn = findViewById(R.id.signIn);

        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                progressDialog.setMessage("Loading");
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("email", emailAddress.getText().toString());
                    jsonObject.put("password", password.getText().toString());
                } catch(JSONException ex) {
                    ex.printStackTrace();
                }

               sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);

                Ion.with(LoginActivity.this)
                        .load(Constants.LOGIN)
                        .setHeader("Accept","application/json")
                        .setHeader("Content-Type","application/json")
                        .setTimeout(60000)
                        .setStringBody(jsonObject.toString())
                        .asString()
                        .setCallback(new FutureCallback<String>() {
                            @Override
                            public void onCompleted(Exception e, String result) {
                                progressDialog.dismiss();
                                String jsonResult = result;
                                System.out.println(jsonResult);
                                try {
                                    JSONObject jsonObject1 = new JSONObject(jsonResult);
                                    if(jsonObject1.has("access_token")) {

                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString("access_token", jsonObject1.getString("access_token"));
                                        editor.putBoolean("logged_in", true);
                                        editor.apply();
                                        Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
                                        startActivity(i);
                                        finish();
                                    } else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                                        builder.setTitle("Error");
                                        builder.setMessage("Access denied please check your login credentials");
                                        builder.setNegativeButton("Exit",
                                                new DialogInterface.OnClickListener()
                                                {
                                                    public void onClick(DialogInterface dialog, int id)
                                                    {
                                                        dialog.cancel();
                                                    }
                                                });
                                        builder.create();
                                        builder.show();
                                    }
                                } catch(JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });

            }
        });
    }
}

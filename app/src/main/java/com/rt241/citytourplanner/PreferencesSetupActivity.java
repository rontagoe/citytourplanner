package com.rt241.citytourplanner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.PreferencesAdapter;
import com.rt241.citytourplanner.Models.PreferencesModel;
import com.rt241.citytourplanner.Utils.Constants;
import com.rt241.citytourplanner.Utils.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.rt241.citytourplanner.Adapters.PreferencesAdapter.selectedPreference;

public class PreferencesSetupActivity extends AppCompatActivity implements  PreferencesAdapter.PreferenceAdapterListener{

    private RecyclerView recyclerview;
    private List<PreferencesModel> preferencesModelList = new ArrayList<>();
    ProgressDialog progressDialog;
    private SearchView searchView;
    private PreferencesAdapter preferencesAdapter;
    private TextView next;
    AlertDialog.Builder builder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences_setup);
        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setMessage("Loading preferences");
        recyclerview = findViewById(R.id.recyclerview);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = findViewById(R.id.searchView);
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.show();
                progressDialog.setMessage("Loading");
                try {
                    SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
                    final JSONArray jsonArray = new JSONArray(selectedPreference);
                    final JSONObject jsonObject = new JSONObject();
                    jsonObject.put("name", sharedPreferences.getString("username",""));
                    jsonObject.put("email", sharedPreferences.getString("email",""));
                    jsonObject.put("password",sharedPreferences.getString("password",""));
                    jsonObject.put("password_confirmation", sharedPreferences.getString("password",""));
                    jsonObject.put("preferences", jsonArray);
                    Ion.with(PreferencesSetupActivity.this)
                            .load("POST",Constants.REGISTER)
                            .setHeader("Content-Type","application/json")
                            .setHeader("Accept","application/json")
                            .setStringBody(jsonObject.toString())
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {
                                    progressDialog.dismiss();
                                    try {
                                        JSONObject jsonObject1 = new JSONObject(result);
                                        if(jsonObject1.has("access_token")) {
                                            SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedPreferences.edit();
                                            editor.putString("access_token", jsonObject1.getString("access_token"));
                                            editor.apply();
                                            Intent i = new Intent(PreferencesSetupActivity.this, DashboardActivity.class);
                                            startActivity(i);
                                        } else {
                                            if(jsonObject1.has("errors")) {
                                                JSONObject jsonObject2 = new JSONObject(jsonObject1.getString("errors"));
                                                if(jsonObject2.has("email")) {
                                                    JSONArray jsonArray1 = jsonObject2.getJSONArray("email");
                                                    builder.setTitle("Error");
                                                    builder.setMessage(jsonArray1.getString(0))
                                                            .setCancelable(false)
                                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    //  Action for 'NO' Button
                                                                    dialog.cancel();

                                                                }
                                                            });
                                                    AlertDialog alert = builder.create();
                                                    alert.show();
                                                }

                                                if(jsonObject2.has("password")) {
                                                    JSONArray jsonArray1 = jsonObject2.getJSONArray("password");
                                                    builder.setTitle("Error");
                                                    builder.setMessage(jsonArray1.getString(0))
                                                            .setCancelable(false)
                                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    //  Action for 'NO' Button
                                                                    dialog.cancel();

                                                                }
                                                            });
                                                    AlertDialog alert = builder.create();
                                                    alert.show();
                                                }

                                                if(jsonObject2.has("name")) {
                                                    JSONArray jsonArray1 = jsonObject2.getJSONArray("name");
                                                    builder.setTitle("Error");
                                                    builder.setMessage(jsonArray1.getString(0))
                                                            .setCancelable(false)
                                                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    //  Action for 'NO' Button
                                                                    dialog.cancel();

                                                                }
                                                            });
                                                    AlertDialog alert = builder.create();
                                                    alert.show();
                                                }
                                            }
                                        }
                                    } catch(JSONException ex) {
                                        progressDialog.dismiss();
                                    }

                                }
                            });
                } catch(JSONException ex) {
                    Log.d("ERROR", ex.getMessage());
                    progressDialog.dismiss();
                }
            }
        });



        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                preferencesAdapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                preferencesAdapter.getFilter().filter(query);
                return true;
            }
        });
        Ion.with(PreferencesSetupActivity.this)
                .load(Constants.TAGS)
                .setLogging("HEY", Log.DEBUG)
                .setHeader("Accept","application/json")
                .setHeader("Content-Type","application/json")
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                               JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                               preferencesModelList.add(new PreferencesModel(jsonObject1.getString("title"), jsonObject1.getInt("id")));
                            }
                            preferencesAdapter = new PreferencesAdapter(preferencesModelList, PreferencesSetupActivity.this);
                            recyclerview.setAdapter(preferencesAdapter);
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                });
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        //selectedPlaces.clear();
//        recyclerview.addOnItemTouchListener(
//                new RecyclerItemClickListener(this, recyclerview ,new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//                        // do whatever
//                    }
//
//                    @Override public void onLongItemClick(View view, int position) {
//                        // do whatever
//                    }
//                })
//        );
    }

    @Override
    public void onPreferenceSelected(PreferencesModel preferencesModel) {

       // System.out.println(preferencesModel.getId()+"ahora puedo ver");
    }
}
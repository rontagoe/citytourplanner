package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.AddPlaceToTourAdapter;
import com.rt241.citytourplanner.Adapters.HomeAdapter;
import com.rt241.citytourplanner.Models.City;
import com.rt241.citytourplanner.Models.HomeModel;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddPlaceToTourActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    SharedPreferences sharedPreferences;
    String token,cityId,tourId;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_place_to_tour);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            cityId = extras.getString("city_id");
            tourId = extras.getString("tour_id");
        }

        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        sharedPreferences = this.getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("access_token", "");

        recyclerView = findViewById(R.id.recyclerview);
        textView = findViewById(R.id.textView2);

        textView.setText("");

        getTourPlaces();
    }

    private void getTourPlaces() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("tour_id", tourId);
        editor.apply();
        Ion.with(this)
                .load("GET", Constants.PLACE+cityId)
                .setHeader("Accept","application/json")
                .setHeader("Content-Type","application/json")
                .setHeader("Authorization","Bearer "+token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        String jsonResult = result.trim();

                        try {
                            List<Place> homeModelList = new ArrayList<>();
                            JSONObject jsonObject1 = new JSONObject(jsonResult);
                            JSONArray jsonArray = jsonObject1.getJSONArray("data");
                            if(jsonArray.length() == 0){
                                textView.setText("No records");
                            }else {
                                textView.setText("");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    int id = jsonObject2.getInt("id");
                                    String name = jsonObject2.getString("name");
                                    String description = jsonObject2.getString("description");
                                    double lat = jsonObject2.getDouble("lat");
                                    double lng = jsonObject2.getDouble("lng");
                                    double rating = jsonObject2.getDouble("rating");//. null ? 0.00 : jsonObject2.getDouble("rating");
                                    String updated_at = jsonObject2.getString("updated_at");
                                    String reviews = jsonObject2.getString("reviews");
                                    String photo = jsonObject2.getString("photo");
                                    Log.e("TESTING",photo+"kdkdkd iieieieiei");
                                    JSONObject city = jsonObject2.getJSONObject("city");
                                    int cityId = city.getInt("id");
                                    String cityName = city.getString("name");
                                    String cityCountry = city.getString("country");

                                    homeModelList.add(new Place(id, name, description, lat, lng, new City(cityId, cityName, cityCountry,"-"), updated_at, rating, reviews, !photo.isEmpty() ? photo : "http://i.imgur.com/DvpvklR.png"));
                                }
                                AddPlaceToTourAdapter homeAdapter = new AddPlaceToTourAdapter(homeModelList, getApplicationContext());
                                recyclerView.setAdapter(homeAdapter);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            }
                            progressDialog.dismiss();
                        } catch(JSONException ex) {
                            textView.setText(ex.getMessage());
                            ex.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                });
    }
}
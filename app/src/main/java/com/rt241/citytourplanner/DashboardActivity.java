package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rt241.citytourplanner.Fragments.FavoriteFragment;
import com.rt241.citytourplanner.Fragments.HomeFragment;
import com.rt241.citytourplanner.Fragments.ProfileFragment;
import com.rt241.citytourplanner.Fragments.ShareFragment;

public class DashboardActivity extends AppCompatActivity {

    Fragment fragment = null;
    FragmentTransaction fragmentTransaction;
    FloatingActionButton fab;
    ImageButton fourth_menu_item, second_menu_item, third_menu_item, first_menu_item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        fab = findViewById(R.id.fab);
        fab.setOnClickListener(v -> {
            Intent i = new Intent(DashboardActivity.this, StartItineraryActivity.class);
            startActivity(i);
        });
        final HomeFragment homeFragment = new HomeFragment();
        final ProfileFragment profileFragment = new ProfileFragment();
        final ShareFragment shareFragment = new ShareFragment();
        final FavoriteFragment favoriteFragment = new FavoriteFragment();
        fourth_menu_item = findViewById(R.id.fourth_menu_item);
        fourth_menu_item.setOnClickListener(v -> {
            first_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_grey));
            second_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_share_grey));
            third_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_grey));
            fourth_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_purple));
            switchFragment(profileFragment);
        });
        second_menu_item = findViewById(R.id.second_menu_item);
        second_menu_item.setOnClickListener(v -> {
            first_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_grey));
            second_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_share_purple));
            third_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_grey));
            fourth_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_grey));
            switchFragment(shareFragment);
        });
        third_menu_item = findViewById(R.id.third_menu_item);
        third_menu_item.setOnClickListener(v -> {
            first_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_grey));
            second_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_share_grey));
            third_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_purple));
            fourth_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_grey));
            switchFragment(favoriteFragment);
        });

        first_menu_item = findViewById(R.id.first_menu_item);
        first_menu_item.setOnClickListener(v -> {
            first_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_purple));
            second_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_share_grey));
            third_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_grey));
            fourth_menu_item.setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_grey));
            switchFragment(homeFragment);
        });
        switchFragment(homeFragment);
    }

    private void switchFragment(Fragment fragment) {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, fragment);
        fragmentTransaction.commit();
    }
}
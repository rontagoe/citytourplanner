package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class UsernameActivity extends AppCompatActivity {
    private EditText username;
    private TextView next;
    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_username);
        sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        username = findViewById(R.id.username);
        next = findViewById(R.id.next);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usernameString = username.getText().toString();

                if(usernameString.isEmpty()){
                    username.setError("Required field");
                }else{
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("username", usernameString);
                    editor.apply();
                    Intent i = new Intent(UsernameActivity.this, PreferencesSetupActivity.class);
                    startActivity(i);
                }

            }
        });
    }
}
package com.rt241.citytourplanner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.ShareAdapter;
import com.rt241.citytourplanner.Adapters.TourPlaceDetailAdapter;
import com.rt241.citytourplanner.Models.City;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.Models.ShareModel;
import com.rt241.citytourplanner.Utils.Constants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.github.ponnamkarthik.richlinkpreview.RichLinkView;
import io.github.ponnamkarthik.richlinkpreview.ViewListener;

public class ItenerayDetailsActivity extends AppCompatActivity {

    String tourId, cityId;
    SharedPreferences sharedPreferences;
    String token;
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    ImageView imagePlace, back;
    TextView cityName,textView1;
    FloatingActionButton fabAddLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iteneray_details);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            tourId = extras.getString("tour_id");
            cityId = extras.getString("city_id");
        }

        back = findViewById(R.id.back);
        imagePlace = findViewById(R.id.imagePlace);
        recyclerView = findViewById(R.id.recyclerview);
        cityName = findViewById(R.id.placeName);
        textView1 = findViewById(R.id.textView1);
        fabAddLocation = findViewById(R.id.fabAddLocation);


        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setMessage("Loading");
        sharedPreferences = getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("access_token", "");

        textView1.setText("");

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        fabAddLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ItenerayDetailsActivity.this, AddPlaceToTourActivity.class);
                i.putExtra("tour_id", tourId);
                i.putExtra("city_id", cityId);
                startActivity(i);
            }
        });

        getUserTourDetails();

    }

    private void getUserTourDetails() {
        Ion.with(this)
                .load("GET", Constants.GET_USER_TOURS + "/" + tourId)
                .setHeader("Accept", "application/json")
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", "Bearer " + token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        String jsonResult = result.trim();
                        System.out.println(result + "Hi");
                        try {
                            List<Place> placelList = new ArrayList<>();
                            JSONObject jsonObject1 = new JSONObject(jsonResult);
                            JSONArray jsonArray = jsonObject1.getJSONArray("data");

                            JSONObject jsonObject2 = jsonArray.getJSONObject(0);
                            System.out.println(jsonObject2.toString() + "ododod");
                            Log.d("JSON:::::", String.valueOf(jsonObject2));
                            int id = jsonObject2.getInt("id");
                            String city = jsonObject2.getString("city");

                            JSONObject jsonObject3 = new JSONObject(city);
                            String cId = jsonObject3.getString("id");
                            String name = jsonObject3.getString("name");
                            String country = jsonObject3.getString("country");
                            String city_photo = jsonObject3.getString("photo");
                            String youtube_links = jsonObject3.getString("youtube_links");

                            Picasso.get().load(city_photo).into(imagePlace);
                            cityName.setText(name);

                            JSONArray placesJsonArray = jsonObject2.getJSONArray("places");
                            if(placesJsonArray.length() == 0) {
                                textView1.setText("No places record");
                            }else {
                                for (int i = 0; i < placesJsonArray.length(); i++) {
                                    JSONObject jsonObject = placesJsonArray.getJSONObject(i);
                                    int place_id = jsonObject.getInt("id");
                                    String place = jsonObject.getString("name");
                                    String description = jsonObject.getString("description");
                                    String photo = jsonObject.getString("photo");

                                    placelList.add(new Place(place_id, place, description, 0.0, 0.0, new City(0, "cityName", "cityCountry","-"), "updated_at", 0.0, "reviews", !photo.isEmpty() ? photo : "http://i.imgur.com/DvpvklR.png"));
                                }
                                TourPlaceDetailAdapter homeAdapter = new TourPlaceDetailAdapter(placelList, ItenerayDetailsActivity.this);
                                recyclerView.setAdapter(homeAdapter);
                                recyclerView.setLayoutManager(new LinearLayoutManager(ItenerayDetailsActivity.this, LinearLayoutManager.HORIZONTAL, false));

                                textView1.setText("");
                            }
                            progressDialog.dismiss();
                        } catch (
                                JSONException ex) {
                            ex.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                });
    }
}
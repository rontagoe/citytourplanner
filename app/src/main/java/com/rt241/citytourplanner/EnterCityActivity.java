package com.rt241.citytourplanner;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EnterCityActivity extends AppCompatActivity {
    private FrameLayout frameLayout;
    private SharedPreferences sharedPreferences;
    EditText place;
    ProgressDialog progressDialog;
    String token;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_city);

        sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        place = findViewById(R.id.place);
        progressDialog = new ProgressDialog(this);
        frameLayout = findViewById(R.id.frameLayout);
        back = findViewById(R.id.back);

        token = sharedPreferences.getString("access_token", "");

//        sharedPreferences = getApplicationContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putString("city", )

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCity();

            }
        });
    }

    private void getCity() {
        progressDialog.show();
        progressDialog.setMessage("Loading");
        Ion.with(EnterCityActivity.this)
                .load("GET", Constants.SEARCH_CITY + place.getText().toString())
                .setHeader("Accept", "application/json")
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", "Bearer " + token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        progressDialog.dismiss();
                        String jsonResult = result.trim();
                        System.out.println(jsonResult);
                        try {
                            JSONObject jsonObject = new JSONObject(jsonResult);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            if (jsonArray.length() > 0) {
                                JSONObject data = jsonArray.getJSONObject(0);
                                Log.d("EI:::::::::::", String.valueOf(jsonArray));
                                String placeID = data.getString("id");
                                String placeName = data.getString("name");
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putString("place_id", placeID);
                                editor.apply();
                                Intent i = new Intent(EnterCityActivity.this, EnterDaysActivity.class);
                                i.putExtra("id", placeID);
                                i.putExtra("placeName", placeName);
                                startActivity(i);
                            } else {
                                Toast.makeText(EnterCityActivity.this, "" + place.getText().toString() + " is currently not profiled on City Tour Planner. We would add it shortly.\nThank you.", Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                });
    }
}
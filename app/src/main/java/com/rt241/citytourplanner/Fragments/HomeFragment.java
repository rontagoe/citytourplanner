package com.rt241.citytourplanner.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.HomeAdapter;
import com.rt241.citytourplanner.DashboardActivity;
import com.rt241.citytourplanner.LoginActivity;
import com.rt241.citytourplanner.Models.City;
import com.rt241.citytourplanner.Models.HomeModel;
import com.rt241.citytourplanner.R;
import com.rt241.citytourplanner.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ProgressDialog progressDialog;
    RecyclerView recyclerView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        progressDialog = new ProgressDialog(getContext());
        progressDialog.show();
        progressDialog.setMessage("Loading");
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("cityPref",Context.MODE_PRIVATE);
        token = sharedPreferences.getString("access_token", "");

        View view =  inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);



        getCities();


        return view;
    }

    private void getCities() {
        Ion.with(this)
                .load("GET", Constants.CITY)
                .setLogging("CITIES", Log.DEBUG)
                .setHeader("Accept","application/json")
                .setHeader("Content-Type","application/json")
                .setHeader("Authorization","Bearer "+token)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        String jsonResult = result.toString().trim();
                        System.out.println(jsonResult);
                        try {
                            List<City> cityList = new ArrayList<>();
                            //  List<HomeModel> homeModelList = new ArrayList<>();
                            JSONObject jsonObject1 = new JSONObject(jsonResult);
                            JSONArray jsonArray = jsonObject1.getJSONArray("data");
                            for(int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                System.out.println(jsonObject2.toString()+"ododod");
                                int id = jsonObject2.getInt("id");
                                String name = jsonObject2.getString("name");
                                String country = jsonObject2.getString("country");
                                String photo = jsonObject2.getString("photo");

                                City city = new City(id,name,country, photo.equals("null") || photo.isEmpty() ?"http://i.imgur.com/DvpvklR.png":photo);
                                cityList.add(city);
                                //   homeModelList.add(new HomeModel(id,place_id,"http://i.imgur.com/DvpvklR.png",c,place));
                            }
                            HomeAdapter homeAdapter = new HomeAdapter(cityList, getContext());
                            recyclerView.setAdapter(homeAdapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                            progressDialog.dismiss();
                        } catch(JSONException ex) {
                            ex.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                });
    }
}
package com.rt241.citytourplanner.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.PreviousTourAdapter;
import com.rt241.citytourplanner.Models.City;
import com.rt241.citytourplanner.Models.PreviousTourModel;
import com.rt241.citytourplanner.PreferencesSetupUpdateActivity;
import com.rt241.citytourplanner.R;
import com.rt241.citytourplanner.SelectActivity;
import com.rt241.citytourplanner.Utils.Constants;
import com.squareup.picasso.Picasso;

import net.indieconsortium.citytourplanner.ProfileUploadActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.rt241.citytourplanner.Adapters.PreferencesAdapter.selectedPreference;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    CircleImageView profile_image;
    ImageView logOut;
    TextView username, preferences;
    SharedPreferences sharedPreferences;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        username = view.findViewById(R.id.username);
        profile_image = view.findViewById(R.id.profile_image);
        preferences = view.findViewById(R.id.preferences);
        logOut = view.findViewById(R.id.logOut);

        sharedPreferences = getContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);

        preferences.setOnClickListener(view12 -> {
            Intent intent = new Intent(getActivity(), PreferencesSetupUpdateActivity.class);
            intent.putIntegerArrayListExtra("selectedPreference", selectedPreference);
            startActivity(intent);
        });

        logOut.setOnClickListener(view1 -> logOut());

        profile_image.setOnClickListener(view13 -> {
            Intent intent = new Intent(getActivity(), ProfileUploadActivity.class);
            startActivity(intent);
        });

      //  getProfile();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getProfile();
    }

    @Override
    public void onStart() {
        super.onStart();
        super.onResume();
    }

    private void getProfile() {

        progressDialog = new ProgressDialog(getContext());
        progressDialog.show();
        progressDialog.setMessage("Loading");

        String token = sharedPreferences.getString("access_token", "");

        Ion.with(this)
                .load("GET", Constants.USER)
                .setLogging("PROFILE", Log.DEBUG)
                .setHeader("Accept", "application/json")
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", "Bearer " + token)
                .asString()
                .setCallback((e, result) -> {

                    String jsonResult = result.trim();

                    try {
                        JSONArray jsonArray = new JSONArray(jsonResult);

                        Log.d("PRO::::::::::", String.valueOf(jsonArray));
                        List<PreviousTourModel> previousTourModelList = new ArrayList<>();


                        if (jsonArray.length() == 0) {
                            //   textView.setText("No records");
                        } else {
                            //  textView.setText("");
                            //  for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject2 = jsonArray.getJSONObject(0);
                            int id = jsonObject2.getInt("id");
                            String name = jsonObject2.getString("name");
                            String email = jsonObject2.getString("email");
                            String photo = jsonObject2.getString("photo");

                            Picasso.get().load(photo.equals("null") || photo.isEmpty() ? "http://i.imgur.com/DvpvklR.png" : photo).into(profile_image);
                            username.setText(name);

                            JSONArray preferenceJsonArray = jsonObject2.getJSONArray("preferences");
                            preferences.setText("");
                            for (int i = 0; i < preferenceJsonArray.length(); i++) {
                                JSONObject jsonObject = preferenceJsonArray.getJSONObject(i);

                                int in = jsonObject.getInt("id");
                                String type = jsonObject.getString("title");


                                preferences.append(type+", ");
                                selectedPreference.add(in);
                            }

                            JSONArray placesJsonArray = jsonObject2.getJSONArray("tours");

                            for (int i = 0; i < placesJsonArray.length(); i++) {
                                JSONObject jsonObject = placesJsonArray.getJSONObject(i);
                                    /*int place_id = jsonObject.getInt("id");
                                    String place = jsonObject.getString("name");
                                    String description = jsonObject.getString("description");
                                    String photo = jsonObject.getString("photo");*/

                                JSONObject city = jsonObject.getJSONObject("city");
                                int cityId = city.getInt("id");
                                String cityName = city.getString("name");
                                String cityCountry = city.getString("country");
                                String cityPhoto = city.getString("photo");

                                previousTourModelList.add(new PreviousTourModel("https://www.bigredcloud.com/wp-content/uploads/4-tips-for-taking-professional-profile-pictures.jpg", "-", !cityName.isEmpty() ? cityName : "-",new City(cityId, cityName, cityCountry, cityPhoto.equals("") || cityPhoto.isEmpty() ? "-": cityPhoto)));
                            }

                            //     homeModelList.add(new Place(id, name, description, lat, lng, new City(cityId, cityName, cityCountry), updated_at, rating, reviews, !photo.isEmpty() ? photo : "http://i.imgur.com/DvpvklR.png"));
                            //       }
                            PreviousTourAdapter homeAdapter = new PreviousTourAdapter(previousTourModelList, getContext());
                            recyclerView.setAdapter(homeAdapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                        }
                        progressDialog.dismiss();
                    } catch (JSONException ex) {
                        //  textView.setText(ex.getMessage());
                        ex.printStackTrace();
                        progressDialog.dismiss();
                    }
                });
    }




    private void logOut(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("logged_in", false);
        editor.apply();
        getActivity().finish();
        Intent intent = new Intent(getActivity(), SelectActivity.class);
        startActivity(intent);
    }
}
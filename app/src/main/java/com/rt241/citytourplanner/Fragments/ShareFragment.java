package com.rt241.citytourplanner.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.HomeAdapter;
import com.rt241.citytourplanner.Adapters.ShareAdapter;
import com.rt241.citytourplanner.Models.City;
import com.rt241.citytourplanner.Models.HomeModel;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.Models.ShareModel;
import com.rt241.citytourplanner.R;
import com.rt241.citytourplanner.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShareFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShareFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    RecyclerView recyclerView;
    ProgressDialog progressDialog;


    public ShareFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ShareFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShareFragment newInstance(String param1, String param2) {
        ShareFragment fragment = new ShareFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    SharedPreferences sharedPreferences;
    String token;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_share, container, false);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.show();
        progressDialog.setMessage("Loading");
        sharedPreferences = getContext().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("access_token", "");
        recyclerView = view.findViewById(R.id.recyclerview);

        getUserTours();
        return view;
    }

    private void getUserTours() {
        Ion.with(getContext())
                .load("GET", Constants.GET_USER_TOURS)
                .setHeader("Accept", "application/json")
                .setHeader("Content-Type", "application/json")
                .setHeader("Authorization", "Bearer " + token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        String jsonResult = result.trim();
                        try {
                            List<ShareModel> shareModelList = new ArrayList<>();
                            JSONObject jsonObject1 = new JSONObject(jsonResult);
                            JSONArray jsonArray = jsonObject1.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                int id = jsonObject2.getInt("id");
                                String start_date = jsonObject2.getString("start_date");
                                String end_date = jsonObject2.getString("end_date");
                                String city = jsonObject2.getString("city");

                                JSONObject jsonObject3 = new JSONObject(city);
                                String cId = jsonObject3.getString("id");
                                String name = jsonObject3.getString("name");
                                String country = jsonObject3.getString("country");
                                String photo = jsonObject3.getString("photo");

                                JSONArray jsonArray1 = jsonObject2.getJSONArray("places");

                                List<Place> placelList = new ArrayList<>();
                                if (jsonArray1 != null && jsonArray1.length() != 0) {

                                    for(int ii =0;ii < jsonArray1.length();ii++){

                                        JSONObject jsonObject = jsonArray1.getJSONObject(ii);
                                        int place_id = jsonObject.getInt("id");
                                        String placeName = jsonObject.getString("name");
                                        String description = jsonObject.getString("description");
                                        String placePhoto = jsonObject.getString("photo");

                                        Log.d("NAME:::::",placeName);

                                        placelList.add(new Place(place_id, placeName, description, 0.0, 0.0, null, "-", 0.0, "-", placePhoto));

                                    }

                               }
                                shareModelList.add(new ShareModel(id, 0, "http://i.imgur.com/DvpvklR.png", "-", new City(Integer.parseInt(cId), name, country, photo.equals("") || photo.isEmpty() ? "-":photo), "-", placelList.size() > 0 ? placelList : null,start_date,end_date));

                                // }

                            }
                            ShareAdapter homeAdapter = new ShareAdapter(shareModelList, getContext());
                            recyclerView.setAdapter(homeAdapter);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                            progressDialog.dismiss();
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                });
    }
}
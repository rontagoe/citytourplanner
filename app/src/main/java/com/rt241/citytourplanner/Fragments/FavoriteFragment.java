package com.rt241.citytourplanner.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.AddPlaceToTourAdapter;
import com.rt241.citytourplanner.Adapters.FavoriteAdapter;
import com.rt241.citytourplanner.Adapters.RecommendedPlaceAdapter;
import com.rt241.citytourplanner.Models.City;
import com.rt241.citytourplanner.Models.Favorite;
import com.rt241.citytourplanner.Models.Place;
import com.rt241.citytourplanner.R;
import com.rt241.citytourplanner.Utils.Constants;
import com.rt241.citytourplanner.helper.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavoriteFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavoriteFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ProgressDialog progressDialog;

    RecyclerView recyclerView;
    SharedPreferences sharedPreferences;
    String token,cityId,tourId;
    TextView textView;


  //  private ArrayList<Favorite> favoriteArrayList;

  //  private FavoriteAdapter favoriteAdapter;

    private DatabaseHelper db;

    public FavoriteFragment() {
        // Required empty public constructor
    }

    public static FavoriteFragment newInstance(String param1, String param2) {
        FavoriteFragment fragment = new FavoriteFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        progressDialog = new ProgressDialog(getContext());


        View view =  inflater.inflate(R.layout.fragment_favorite, container, false);

        sharedPreferences = getActivity().getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        token = sharedPreferences.getString("access_token", "");
        recyclerView = view.findViewById(R.id.recyclerview);
        textView = view.findViewById(R.id.textView2);

        db = new DatabaseHelper(getActivity());

      //  favoriteArrayList = new ArrayList<>();

      //  favoriteAdapter = new FavoriteAdapter(favoriteArrayList, getActivity());

        doFavourite();
        return view;
    }

    private void doFavourite() {
       // Cursor cursor = db.getFavourite();
       // if(String.valueOf(cursor).equals("0")) {

        //    loadFavourite();
      //  }else {
        progressDialog.show();
        progressDialog.setMessage("Loading");
        getRecommendedPlaces();
        //}
    }
    private void loadFavourite() {
      //  favoriteArrayList.clear();
        Cursor cursor = db.getFavourite();
        if (cursor.moveToFirst()) {
            do {
                Favorite n = new Favorite(cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getInt(cursor.getColumnIndex("user_id")),
                        cursor.getInt(cursor.getColumnIndex("place_id")),
                        cursor.getString(cursor.getColumnIndex("place")),
                        cursor.getString(cursor.getColumnIndex("city")));

    //            favoriteArrayList.add(n);
                refreshList();
            } while (cursor.moveToNext());
        }
        refreshList();

        Log.d("CUR", String.valueOf(cursor));
   //     Log.d("favouriteArrayList", String.valueOf(favoriteArrayList));


    }

    private void getRecommendedPlaces() {

        Ion.with(this)
                .load("GET", Constants.RECOMMENDED_PLACES)
                .setLogging("RECOMMENDED", Log.DEBUG)
                .setHeader("Accept","application/json")
                .setHeader("Content-Type","application/json")
                .setHeader("Authorization","Bearer "+token)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        String jsonResult = result.trim();

                        Log.d("PRO::::::::::", result);

                        try {
                            List<Place> homeModelList = new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(jsonResult);
                          //  JSONObject jsonArray = jsonObject1.getJSONObject(0);
                            if(jsonArray.length() == 0){
                                textView.setText("No records");
                            }else {
                                textView.setText("");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                    int id = jsonObject2.getInt("id");
                                    String name = jsonObject2.getString("name");
                                    String description = jsonObject2.getString("description");
                                    double lat = jsonObject2.getDouble("lat");
                                    double lng = jsonObject2.getDouble("lng");
                                    double rating = jsonObject2.getDouble("rating");//. null ? 0.00 : jsonObject2.getDouble("rating");
                                    String updated_at = jsonObject2.getString("updated_at");
                                    String reviews = jsonObject2.getString("reviews");
                                    String photo = jsonObject2.getString("photo");
                                    JSONObject city = jsonObject2.getJSONObject("city");
                                    int cityId = city.getInt("id");
                                    String cityName = city.getString("name");
                                    String cityCountry = city.getString("country");
                                    homeModelList.add(new Place(id, name, description, lat, lng, new City(cityId, cityName, cityCountry,"-"), updated_at, rating, reviews, !photo.isEmpty() ? photo : "http://i.imgur.com/DvpvklR.png"));
                                }
                                System.out.println(homeModelList.size()+"homeModelList");
                                RecommendedPlaceAdapter recommendedPlaceAdapter = new RecommendedPlaceAdapter(homeModelList, getActivity());
                                recyclerView.setAdapter(recommendedPlaceAdapter);
                                recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                            }
                            progressDialog.dismiss();
                        } catch(JSONException ex) {
                            textView.setText(ex.getMessage());
                            ex.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }
                });
    }

    private void refreshList() {
     //   favoriteAdapter.notifyDataSetChanged();
        progressDialog.dismiss();
      //  toggleView();
    }
}
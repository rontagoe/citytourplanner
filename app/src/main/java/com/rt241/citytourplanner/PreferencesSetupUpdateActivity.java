package com.rt241.citytourplanner;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.Adapters.PreferencesAdapter;
import com.rt241.citytourplanner.Models.PreferencesModel;
import com.rt241.citytourplanner.Utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.rt241.citytourplanner.Adapters.PreferencesAdapter.selectedPreference;

public class PreferencesSetupUpdateActivity extends AppCompatActivity implements  PreferencesAdapter.PreferenceAdapterListener{

    private RecyclerView recyclerview;
    private List<PreferencesModel> preferencesModelList = new ArrayList<>();
    ProgressDialog progressDialog;
    private SearchView searchView;
    private PreferencesAdapter preferencesAdapter;
    private TextView next;
    AlertDialog.Builder builder;
    String token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences_setup);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            selectedPreference = extras.getIntegerArrayList("selectedPreference");
        }

        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setMessage("Loading preferences");
        recyclerview = findViewById(R.id.recyclerview);

        SharedPreferences sharedPreferences = getSharedPreferences("cityPref",Context.MODE_PRIVATE);
        token = sharedPreferences.getString("access_token", "");


        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = findViewById(R.id.searchView);
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        next = findViewById(R.id.next);
        next.setOnClickListener(v -> {
            progressDialog.show();
            progressDialog.setMessage("Loading");

            updatePerferences();
        });



        // listening to search query text change
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // filter recycler view when query submitted
                preferencesAdapter.getFilter().filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // filter recycler view when text is changed
                preferencesAdapter.getFilter().filter(query);
                return true;
            }
        });
        Ion.with(PreferencesSetupUpdateActivity.this)
                .load(Constants.TAGS)
                .setLogging("HEY", Log.DEBUG)
                .setHeader("Accept","application/json")
                .setHeader("Content-Type","application/json")
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {

                        progressDialog.dismiss();

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArray.length(); i++) {
                               JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                               preferencesModelList.add(new PreferencesModel(jsonObject1.getString("title"), jsonObject1.getInt("id")));
                            }
                            preferencesAdapter = new PreferencesAdapter(preferencesModelList, PreferencesSetupUpdateActivity.this);
                            recyclerview.setAdapter(preferencesAdapter);
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                        }

                    }
                });
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        //selectedPlaces.clear();
//        recyclerview.addOnItemTouchListener(
//                new RecyclerItemClickListener(this, recyclerview ,new RecyclerItemClickListener.OnItemClickListener() {
//                    @Override public void onItemClick(View view, int position) {
//                        // do whatever
//                    }
//
//                    @Override public void onLongItemClick(View view, int position) {
//                        // do whatever
//                    }
//                })
//        );
    }

    @Override
    public void onPreferenceSelected(PreferencesModel preferencesModel) {

       // System.out.println(preferencesModel.getId()+"ahora puedo ver");
    }

    private void updatePerferences() {
        JSONObject json = new JSONObject();
        final JSONArray jsonArray = new JSONArray(selectedPreference);
        try {
            json.put("tags",jsonArray);
            Ion.with(this)
                    .load("PATCH", Constants.PREFERENCE)
                    .setLogging("CITIES", Log.DEBUG)
                    .setHeader("Accept","application/json")
                    .setHeader("Content-Type","application/json")
                    .setHeader("Authorization","Bearer "+token)
                    .setStringBody(String.valueOf(json))
                    .asJsonObject()
                    .withResponse()
                    .setCallback((e, result) -> {
                        if(result.getHeaders().code() == 200){
                            finish();
                            Toast.makeText(this, "Preferences Updated Successfully", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(this, "Sorry something went wrong. Please try again later", Toast.LENGTH_SHORT).show();
                        }
                        progressDialog.dismiss();
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
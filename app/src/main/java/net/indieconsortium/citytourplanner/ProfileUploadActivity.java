package net.indieconsortium.citytourplanner;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.koushikdutta.ion.Ion;
import com.rt241.citytourplanner.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;

import static com.rt241.citytourplanner.Utils.Constants.PICTURE_UPLOAD;

public class ProfileUploadActivity extends AppCompatActivity {

    Button btnTakeImage;
    Button btnRetake;
    Button btnUpload;

    ImageView imageViewDocument;

    Bitmap thumbnail;

    final int PERMISSIONS_MULTIPLE_REQUEST = 999;

    String imagePath = "";

    ConstraintLayout imageDisplayVisibility;


    //SharedPreferences prefs;
    SharedPreferences.Editor edit;
    ProgressDialog progressDialog;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_upload);

        checkAndroidVersion();


        btnTakeImage = findViewById(R.id.btnTakeImage);
        imageDisplayVisibility = findViewById(R.id.imageDisplayVisibility);

        imageViewDocument = findViewById(R.id.imageViewDocument);

        sharedPreferences = getSharedPreferences("cityPref", Context.MODE_PRIVATE);
        edit = sharedPreferences.edit();


        btnRetake = findViewById(R.id.btnRetake);
        btnUpload = findViewById(R.id.btnUpload);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);


        btnTakeImage.setOnClickListener(v -> {

            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, PERMISSIONS_MULTIPLE_REQUEST);
        });

        btnRetake.setOnClickListener(v -> {
            btnTakeImage.setVisibility(View.VISIBLE);
            imageDisplayVisibility.setVisibility(View.GONE);
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, PERMISSIONS_MULTIPLE_REQUEST);
        });

        btnUpload.setOnClickListener(v -> {

            validationDocument();
        });
    }


    private void checkAndroidVersion() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();

        } else {
            // write your logic here
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requestCode == PERMISSIONS_MULTIPLE_REQUEST && resultCode == RESULT_OK && data != null) {

                onCaptureImageResult(data);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermission();

        } else {
            // write your logic here
            onCaptureImageResult(data);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_MULTIPLE_REQUEST) {
            if (grantResults.length > 0) {
                boolean cameraPermission = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                boolean writeExternalFile = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                boolean readExternalFile = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (cameraPermission && writeExternalFile && readExternalFile) {
                    // write your logic here
                } else {
                    Snackbar.make(this.findViewById(android.R.id.content),
                            "Please Grant Permissions to upload profile photo",
                            Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                            v -> {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(
                                            new String[]{Manifest.permission
                                                    .READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                            PERMISSIONS_MULTIPLE_REQUEST);
                                }
                            }).show();
                }
            }
        }
    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");

        Uri tempUri = getImageUri(getApplicationContext(), thumbnail);

        imagePath = getRealPathFromURI(tempUri);

        System.out.println(getRealPathFromURI(tempUri));


        //set Image from Camera
        File image_file = new File(imagePath);
        if (image_file.exists()) {
            btnTakeImage.setVisibility(View.GONE);
            imageDisplayVisibility.setVisibility(View.VISIBLE);
            Picasso.get().load(new File(imagePath)).into(imageViewDocument);
            edit.putString("image_string", imagePath);
            edit.commit();
        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(ProfileUploadActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(ProfileUploadActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(ProfileUploadActivity.this,
                        Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (ProfileUploadActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (ProfileUploadActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (ProfileUploadActivity.this, Manifest.permission.CAMERA)) {

                Snackbar.make(ProfileUploadActivity.this.findViewById(android.R.id.content),
                        "Please Grant Permissions to upload profile photo",
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        v -> {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(
                                        new String[]{Manifest.permission
                                                .READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                        PERMISSIONS_MULTIPLE_REQUEST);
                            }
                        }).show();
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                            new String[]{Manifest.permission
                                    .READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                            PERMISSIONS_MULTIPLE_REQUEST);
                }
            }
        } else {
            // write your logic code if permission already granted
        }
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    private void validationDocument() {
        progressDialog.show();
        progressDialog.setMessage("Loading");

        String token = sharedPreferences.getString("access_token", "");

        File imageFile = new File(sharedPreferences.getString("image_string", ""));

        Ion.with(this)
                .load("POST", PICTURE_UPLOAD)
                .setLogging("Document Logs", Log.DEBUG)
                .setHeader("Authorization", "Bearer " + token)
                .setMultipartFile("photo", imageFile)
                .asJsonObject()
                .withResponse()
                .setCallback((e, result) -> {
                    if (result.getHeaders().code() == 200) {
                        try {
                            finish();
                            Toast.makeText(this, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();


                        } catch (Exception ex) {
                            ex.printStackTrace();
                            progressDialog.dismiss();
                        }
                    } else {
                        try {
                            String jsonString = result.getResult().toString();
                            JSONObject jsonObject = new JSONObject(jsonString);
                            progressDialog.dismiss();
                            Toast.makeText(this, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException ex) {
                            progressDialog.dismiss();

                            ex.printStackTrace();
                        }


                    }
                });

    }
}